package guestbook;

/**
 * Simple class taking details of the guestbook entry and storing them 
 * in an object.
 * @author KasiaBM
 * @version May 28, 2015
 */

public class GuestbookEntry {
    //instance variables
    private String guestName;
    private String guestEmail;
    private String guestMessage;
    
    /**
     * Zero argument constructor that constructs a GuestbookEntry object 
     * with all attributes set to empty strings.
     */
    public GuestbookEntry() {
        guestName = new String();
        guestEmail = new String();
        guestMessage = new String();
    } //end zero-argument constructor GuestbookEntry
    
    /**
     * Three-argument constructor that constructs a GuestbookEntry object 
     * and sets all attributes to the value of arguments.
     * @param name String object representing guest's name
     * @param email String object representing guest's email
     * @param message String object representing guest's message
     */
    public GuestbookEntry(String name, String email, String message) {
        guestName = name;
        guestEmail = email;
        guestMessage = message;
    } //end three-argument constructor GuestbookEntry
    
    //instance setters
    /**
     * Sets the value of the guest's name attribute to the value of the argument.
     * @param name String object representing guest's name
     */
    public void setGuestName(String name) {
        guestName = name;
    } //end method setGuestName
    
    /**
     * Sets the value of the guest's email attribute to the value of the argument.
     * @param email String object representing guest's email
     */
    public void setGuestEmail(String email) {
        guestEmail = email;
    } //end method setGuestEmail
    
    /**
     * Sets the value of the guest's message attribute to the value of the argument.
     * @param message String representing guest's message
     */
    public void setGuestMessage(String message) {
        guestMessage = message;
    } //end method setGuestMessage
    
    //instance getters
    /**
     * Returns the guest's name.
     * @return String object representing guest's name
     */
    public String getGuestName() {
        return guestName;
    } //end method getGuestName
    
    /**
     * Returns the guest's email.
     * @return String object representing guest's email
     */
    public String getGuestEmail() {
        return guestEmail;
    } //end method getGuestEmail
    
    /**
     * Returns the guest's message.
     * @return String object representing guest's message
     */
    public String getGuestMessage() {
        return guestMessage;
    } //end method getGuestMessage
    
    /**
     * Returns String representation of the guest entry object - name, email 
     * and message.
     * @return String object representing guest's name, email and message 
     */
    @Override
    public String toString() {
        return String.format("%s from %s:\n%s", getGuestName(), getGuestEmail(),
                getGuestMessage());
    } //end method toString
} //end class GuestbookEntry