package guestbook;

import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 * Simple managed bean that obtains entry details for the guestbook, stores them
 * in GuestbookEntry objects and returns their list.
 * @author KasiaBM
 * @version June 3, 2015
 */
@ManagedBean(name="guestbookBean")
@ApplicationScoped
public class GuestbookBean implements Serializable {
   
     //instance variables
    private static final ArrayList<GuestbookEntry> guestbookEntries = new ArrayList();
    private String name;
    private String email;
    private String message;
    
    /**
     * Zero-argument constructor that creates a new instance of GuestbookBean.
     */
    public GuestbookBean() {
    } //end zero-argument constructor GuestbookBean
    
    //setters
    /**
     * Creates new guestbook entry object and appends it to the top of the list
     * of entries.
     */
    public void setGuestbookEntry() {
        GuestbookEntry guestbookEntry = new GuestbookEntry(name, email, message);
        guestbookEntries.add(0, guestbookEntry);
    } //end method setGuestbookEntry
    
    /**
     * Sets the value of the guest's name attribute to the value of the argument.
     * @param aName String object representing guest's name
     */
    public void setName(String aName) {
        name = aName;
    } //end method setName
    
    /**
     * Sets the value of the guest's email attribute to the value of the argument.
     * @param anEmail String object representing guest's email
     */
    public void setEmail(String anEmail) {
        email = anEmail;
    } //end method setEmail

    /**
     * Sets the value of the guest's message attribute to the value of the argument.
     * @param aMessage String representing guest's message
     */
    public void setMessage(String aMessage) {
        message = aMessage;
    } //end method setMessage
    
    /**
     * Resets all fields in the form to empty.
     */
    public void resetFields() {
        name = "";
        email = "";
        message = "";
    } //end method resetFields
    
    //getters
    /**
     * Returns the list of guestbook entry objects.
     * @return 
     */
    public ArrayList getGuestbookEntries() {
        return guestbookEntries;
    } //end method getGuestbookEntries
    
    /**
     * Returns the guest's name.
     * @return String object representing guest's name
     */
    public String getName() {
        return name;
    } //end method getName
    
    /**
     * Returns the guest's email.
     * @return String object representing guest's email
     */
    public String getEmail() {
        return email;
    } //end method getEmail
    
    /**
     * Returns the guest's message.
     * @return String object representing guest's message
     */
    public String getMessage() {
        return message;
    } //end method getMessage
} //end class GuestbookBean